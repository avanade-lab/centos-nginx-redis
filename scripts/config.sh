echo =====================================================================================================================================================================
echo PROCESSO INSTALAÇÃO E ATUALIZAÇÃO CENTOS 
echo =====================================================================================================================================================================
echo Iniciando Atualização do CentOS
yum -y update
echo Instalando EPEL-RELEASE
yum -y install epel-release
echo Instalando Dependencias
yum -y install nano
yum -y install wget
echo Instalando Ferramentas
yum -y install openssl-devel
yum -y groupinstall "Development Tools"
yum -y install gettext-devel openssl-devel perl-CPAN perl-devel zlib-devel
yum -y install net-tools
echo =====================================================================================================================================================================
echo PROCESSO INSTALAÇÃO DE FERRAMENTAS 
echo =====================================================================================================================================================================
echo Instalando Redis
yum -y install redis
echo Inicializando e ativando Redis
systemctl start redis.service
systemctl status redis.service
systemctl enable redis.service
echo Verificando Redis
redis-server -v
redis-cli -v
echo Instalando Nginx
yum -y install nginx
echo Inicializando e ativando Nginx
systemctl start nginx
systemctl status nginx
systemctl enable nginx
echo Verificando Atualização Nginx
nginx -v
nginx -t
echo Atualizando Nginx
cp /home/scripts/nginx.repo /etc/yum.repos.d/nginx.repo
echo Verificando Nginx
yum -y update
yum -y install nginx
nginx -v
nginx -t
echo Preparando Nginx-Module
cd /home
echo Baixando fontes Nginx-1.19.10
wget http://nginx.org/download/nginx-1.19.10.tar.gz
echo Descompactando Nginx-1.19.10
tar -xzvf nginx-1.19.10.tar.gz
echo Baixando Nginx-Module-Redis
wget https://people.freebsd.org/~osa/ngx_http_redis-0.3.9.tar.gz
echo Descompactando Nginx-Module-Redis
tar -xzvf ngx_http_redis-0.3.9.tar.gz
echo Preparando compilação Nginx-Module-Redis
cd /home/nginx-1.19.10
./configure --prefix=/etc/nginx --sbin-path=/usr/sbin/nginx --modules-path=/usr/lib/nginx/modules --conf-path=/etc/nginx/nginx.conf --error-log-path=/var/log/nginx/error.log --http-log-path=/var/log/nginx/access.log --pid-path=/var/run/nginx.pid --lock-path=/var/run/nginx.lock --http-client-body-temp-path=/var/cache/nginx/client_temp --http-proxy-temp-path=/var/cache/nginx/proxy_temp --http-fastcgi-temp-path=/var/cache/nginx/fastcgi_temp --http-uwsgi-temp-path=/var/cache/nginx/uwsgi_temp --http-scgi-temp-path=/var/cache/nginx/scgi_temp --user=nginx --group=nginx --with-compat --with-file-aio --with-threads --with-http_addition_module --with-http_auth_request_module --with-http_dav_module --with-http_flv_module --with-http_gunzip_module --with-http_gzip_static_module --with-http_mp4_module --with-http_random_index_module --with-http_realip_module --with-http_secure_link_module --with-http_slice_module --with-http_ssl_module --with-http_stub_status_module --with-http_sub_module --with-http_v2_module --with-mail --with-mail_ssl_module --with-stream --with-stream_realip_module --with-stream_ssl_module --with-stream_ssl_preread_module --with-cc-opt='-g -O2 -fstack-protector-strong -Wformat -Werror=format-security -Wp,-D_FORTIFY_SOURCE=2 -fPIC' --with-ld-opt='-Wl,-Bsymbolic-functions -Wl,-z,relro -Wl,-z,now -Wl,--as-needed -pie' --add-dynamic-module=/home/ngx_http_redis-0.3.9
echo Compilando Nginx-Module-Redis
make modules
echo Detalhes /home/nginx-1.19.10/objs/ngx_http_redis_module.so
ls -la /home/nginx-1.19.10/objs
echo Copiand ngx_http_redis_module.so para /etc/nginx/modules/
cp /home/nginx-1.19.10/objs/ngx_http_redis_module.so /etc/nginx/modules/
echo Atualizando arquivos de Configuração Nginx
# /usr/share/nginx/html
yes | cp -v /home/scripts/nginx.conf /etc/nginx/
echo Re-inicializando Nginx
nginx -s reload
nginx -t
nginx -v
echo =====================================================================================================================================================================
echo PROCESSO CONCLUIDO
echo =====================================================================================================================================================================

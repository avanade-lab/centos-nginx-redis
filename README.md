# *centos-nginx-redis*

Projeto de criação de um container com CentOS 7 com Nginx + Redis + Modulo-Nginx-Redis

## Resumo ##

Este projeto cria um container rodando CentOS 7 com Nginx e Redis.


## Download ##

Baixe o projeto em sua maquina e siga os passos de execução abaixo


## Executar ##

Linux

> $ ./build.sh


Windows

> .\build.bat

## Informações ##

Baixe o conteúdo e siga os passos de inicialização conforme o seu sistema operacional e observe os logs de saída.

## Arquivos ##


| Arquivo | Info | Comentários |
| --- | --- | --- |
| build.bat | Arquivo Batch Windows | Inicialização para Windows  |  
| build.sh | Arquivo Shellscript Linux | Inicialização para Linux |  
| Dockerfile | Arquivo Dockerfile config | Configuração de construição Docker |  
| README.md | Arquivo README  | Leiame |  
| scripts/config.sh | Arquivo Shellscript config | Configuração do Linux pós inicialização do container |  
| scripts/nginx.conf | Arquivo Nginx config | Configuração do Nginx |  
| scripts/nginx.repo | Arquivo Linux config | Configuração do Linux |  


## Sobre o Dockerfile ##

> FROM centos:7
> 
> MAINTAINER "Administrator" <admin@localhost>
> ENV container docker
> RUN yum -y update; yum clean all
> RUN yum -y install systemd; yum clean all; \
> (cd /lib/systemd/system/sysinit.target.wants/; for i in *; do [ $i == > systemd-tmpfiles-setup.service ] || rm -f $i; done); \
> rm -f /lib/systemd/system/multi-user.target.wants/*;\
> rm -f /etc/systemd/system/*.wants/*;\
> rm -f /lib/systemd/system/local-fs.target.wants/*; \
> rm -f /lib/systemd/system/sockets.target.wants/*udev*; \
> rm -f /lib/systemd/system/sockets.target.wants/*initctl*; \
> rm -f /lib/systemd/system/basic.target.wants/*;\
> rm -f /lib/systemd/system/anaconda.target.wants/*;
> VOLUME [ "/sys/fs/cgroup" ]
> CMD ["/usr/sbin/init"]
> 
> WORKDIR home/
> 
> COPY ${UP_FILE} /home

## Sobre o Config.sh ##

> yum -y update\
> yum -y install epel-release\
> yum -y install nano\
> yum -y install wget\
> yum -y install openssl-devel\
> yum -y groupinstall "Development Tools"\
> yum -y install gettext-devel openssl-devel perl-CPAN perl-devel zlib-devel\
> yum -y install net-tools\
> yum -y install redis\
> systemctl start redis.service\
> systemctl status redis.service\
> systemctl enable redis.service\
> redis-server -v\
> redis-cli -v\
> yum -y install nginx\
> systemctl start nginx\
> systemctl status nginx\
> systemctl enable nginx\
> nginx -v\
> nginx -t\
> cp /home/scripts/nginx.repo /etc/yum.repos.d/nginx.repo\
> yum -y update\
> yum -y install nginx\
> nginx -v\
> nginx -t\
> cd /home\
> wget http://nginx.org/download/nginx-1.19.10.tar.gz\
> tar -xzvf nginx-1.19.10.tar.gz\
> wget https://people.freebsd.org/~osa/ngx_http_redis-0.3.9.tar.gz\
> tar -xzvf ngx_http_redis-0.3.9.tar.gz\
> cd /home/nginx-1.19.10\
./configure --prefix=/etc/nginx --sbin-path=/usr/sbin/nginx --modules-path=/usr/lib/nginx/modules --conf-path=/etc/nginx/nginx.conf --error-log-path=/var/log/nginx/error.log --http-log-path=/var/log/nginx/access.log --pid-path=/var/run/nginx.pid --lock-path=/var/run/nginx.lock --http-client-body-temp-path=/var/cache/nginx/client_temp --http-proxy-temp-path=/var/cache/nginx/proxy_temp --http-fastcgi-temp-path=/var/cache/nginx/fastcgi_temp --http-uwsgi-temp-path=/var/cache/nginx/uwsgi_temp --http-scgi-temp-path=/var/cache/nginx/scgi_temp --user=nginx --group=nginx --with-compat --with-file-aio --with-threads --with-http_addition_module --with-http_auth_request_module --with-http_dav_module --with-http_flv_module --with-http_gunzip_module --with-http_gzip_static_module --with-http_mp4_module --with-http_random_index_module --with-http_realip_module --with-http_secure_link_module --with-http_slice_module --with-http_ssl_module --with-http_stub_status_module --with-http_sub_module --with-http_v2_module --with-mail --with-mail_ssl_module --with-stream --with-stream_realip_module --with-stream_ssl_module --with-stream_ssl_preread_module --with-cc-opt='-g -O2 -fstack-protector-strong -Wformat -Werror=format-security -Wp,-D_FORTIFY_SOURCE=2 -fPIC' --with-ld-opt='-Wl,-Bsymbolic-functions -Wl,-z,relro -Wl,-z,now -Wl,--as-needed -pie' --add-dynamic-module=/home/ngx_http_redis-0.3.9\
> make modules\
> ls -la /home/nginx-1.19.10/objs\
> cp /home/nginx-1.19.10/objs/ngx_http_redis_module.so /etc/nginx/modules/\
> yes | cp -v /home/scripts/nginx.conf /etc/nginx/\
> nginx -s reload\
> nginx -t\
> nginx -v